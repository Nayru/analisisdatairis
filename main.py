# importar 
import tensorflow as tf
import numpy as np
from DataTraining import DataTraining

## # lectura de archivos 
#data=DataTraining("iris.txt",60)
data = DataTraining("/home/dcerda/Documentos/InteligenciaArtificial/AnalisisIrisData/iris.txt",60)

Train = np.asarray(data.TrainingData)
Test = np.asarray(data.TestData)

#Lee un archivo que contiene la arquitectura (entradas, neuronas y salidas)
NNarchitect = np.loadtxt("NeuralArchitecture.txt")

 # Guardamos todos menos el ultimo dato que es la salida
TrainInputs = Train[:,0:-1]
TestInputs = Test[:,0:-1]
 # guarda el ultimo dato de cada arreglo de la matriz pero en diferentes arreglos para tener la transpuesta para conciderarlos como  salidas
TrainOutputs = np.transpose(np.asmatrix(Train[:,-1]))
TestOutputs = np.transpose(np.asmatrix(Test[:,-1]))

#Imprimi las matrices de entrenamiento
#print (TrainInputs)
#print (TrainOutputs)

# Cuenta el numero de elementos que tiene el archivo
N_TRAINING = len(Train)
N_TEST = len(Test)
# Convierte y guarda como enteros los elementos de la arquitectura
N_INPUT_NODES = int(NNarchitect[0])
N_HIDDEN_NODES = int(NNarchitect[1])
N_OUTPUT_NODES = int(NNarchitect[2])

#print (N_TRAINING)
#print (N_INPUT_NODES)
#print (N_HIDDEN_NODES)
#print (N_OUTPUT_NODES)

# Se crean 2 tensores que no pueden evaluarse por si mismos de tipo flotante, 
#  alimentados por una matriz con el tamaño de la matriz de entrenamiento y el numero de entrada y los de salida
#  con el nombre "x-input" y "y-input"
x_ = tf.placeholder(tf.float32, shape=[N_TRAINING, N_INPUT_NODES], name="x-input")
y_ = tf.placeholder(tf.float32, shape=[N_TRAINING, N_OUTPUT_NODES], name="y-input")


######## Apartit de aqui es donde debo rebizar a detalle ##########


# Se declara un objeto tipo variable 
# Se inicializa con un valor aleatorio uniforme con el formato de un areglo con dimenciones de el numero de nodos de entrada y el numero de nodos ocultos
# el numero aleatorio puede estar entre el -1.0 y el 1.0 sin incluir el ultimo
theta1 = tf.Variable(tf.random_uniform([N_INPUT_NODES,N_HIDDEN_NODES], -1.0, 1.0), name="theta1")
theta2 = tf.Variable(tf.random_uniform([N_HIDDEN_NODES,N_OUTPUT_NODES], -1.0, 1.0), name="theta2")

#Se declara un tensor en ceros con la forma en el numero de nodos de salida o los ocultos
bias1 = tf.Variable(tf.zeros([N_HIDDEN_NODES]), name="bias1")
bias2 = tf.Variable(tf.zeros([N_OUTPUT_NODES]), name="bias2")

# Se guarda el resulado de una evaluasion sigmoidal y = 1 / (1 + exp(-x)) al resultado de la multiplicacion de nuestro primer tensor x_ por theta1 adiriendole a esto el tensor en ceros bias1
layer1 = tf.sigmoid(tf.matmul(x_, theta1) + bias1)
# Como output hacemos otra ealuacion sigmoidal con el producto de la matriz layer que acabamos de obtener por theta2 mas bias2
output = tf.sigmoid(tf.matmul(layer1, theta2) + bias2)

# Se cambian los inputs para hacer el entrenamiento
# Por el cambio de x y y se modifica tambien la construccion de  los ouputs
##For the trained ANN##
x_test = tf.placeholder(tf.float32, shape=[N_TEST, N_INPUT_NODES], name="xtest-input")
y_test = tf.placeholder(tf.float32, shape=[N_TEST, N_OUTPUT_NODES], name="ytest-input")
layer1_test = tf.sigmoid(tf.matmul(x_test, theta1) + bias1)
output_test = tf.sigmoid(tf.matmul(layer1_test, theta2) + bias2)
##

## To save the trained ANN ##
# Se guardan las variables
tf.add_to_collection('vars', theta1)
tf.add_to_collection('vars', theta2)
tf.add_to_collection('vars', bias1)
tf.add_to_collection('vars', bias2)
# Se guardan los parametros del entrenamiento
saver = tf.train.Saver()
##

# Para calcular un costo se reduce la media del cuadrado de las diferencias de las salidas de entrenamiento menos las salidas reales 
# Para este caso ya que tenemos etiquetas
cost = tf.reduce_mean(tf.square(TrainOutputs - output)) 
# Aplica gradientes que minimizan el costo antes obtenido en 0.5
train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cost)

# Se inicializan las variables
init = tf.global_variables_initializer()
# Se abre una nueva sesion y se corre
sess = tf.Session()
sess.run(init)

# Se corre 10,000 veces para entrenar la red
for s in range(10000):
    sess.run(train_step, feed_dict={x_: TrainInputs, y_: TrainOutputs})
    if s % 1000 == 0:
           print('Cost ', sess.run(cost, feed_dict={x_: TrainInputs, y_: TrainOutputs}))

## Test on training data##
print('Test on training data', sess.run(output_test, feed_dict={x_test: TestInputs, y_test: TestOutputs}))

## Save the trained model ##
saver.save(sess, 'TrainedEasyNet')

