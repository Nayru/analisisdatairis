from sklearn.utils import shuffle

## [ buscar una manera de optimizar ] ##
# este metodo ayuda a leer y transformar la base de datos en un arreglo
#    Asume que el archivo se separa por comas y que la ultima columna contiene un string
class DataTraining():
    def reader(self, fileName ):
        file = open(fileName, "r")
        matriz = []
        line_counter = 0
        for line in file:
            linea_matriz = line.split(",")
            matriz.append([])
            for value in range(0, 5):
                if(value < 4):
                    matriz[line_counter].append(float(linea_matriz[value]))
                else:
                    matriz[line_counter].append(linea_matriz[value][:-1])
            line_counter += 1
           # print ("line_counter: ",line_counter,"value: ",value)
        return matriz
    
    def __init__(self,fileName,nEntrena):
        data=shuffle(self.reader(fileName))
        n=int((nEntrena/100)*len(data))
        print (len(data))
        self.TrainingData=data[:n]
        self.TestData= data[n:]
        #return {'TrainingData':TrainingData, 'TestData':TestData}


#data=DataTraining("/home/dcerda/Documentos/InteligenciaArtificial/AnalisisIrisData/iris.txt",60)
#data
#data.TrainingData
#data.TestData
