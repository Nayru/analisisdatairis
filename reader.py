def reader( filename ):
	file = open(filename, "r")
	matriz = []
	line_counter = 0
	for line in file:
		linea_matriz = line.split(",")
		matriz.append([])
		for value in range(0, 5):
			if(value < 4):
				matriz[line_counter].append(float(linea_matriz[value]))
			else:
				matriz[line_counter].append(linea_matriz[value][:-1])
		line_counter += 1
	return matriz
